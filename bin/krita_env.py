#!/usr/bin/python3

import os

def initialize_environment():
    krita_dir = os.getenv("KRITA_DIR", "/var/krita")
    krita_build_dir = os.getenv("KRITA_BUILD_DIR", "/var/krita/build")
    krita_install_dir = os.getenv("KRITA_INSTALL_DIR", "/var/krita/install")
    krita_deps_dir = os.getenv("KRITA_DEPS_DIR", "/var/krita/deps/usr")

    def add_paths_to_env(env, paths = []):
        for path in paths:
            value = os.getenv(env)
            if value:
                os.environ[env] = path + os.pathsep + os.getenv(env)
            else:
                os.environ[env] = path

    add_paths_to_env( "PATH", [os.path.join(krita_install_dir, 'bin'),
                               os.path.join(krita_deps_dir, 'bin')] )
    
    add_paths_to_env( "LD_LIBRARY_PATH", [os.path.join(krita_install_dir, "lib"),
                                          os.path.join(krita_install_dir, "lib64"),
                                          os.path.join(krita_deps_dir, "lib"),
                                          os.path.join(krita_deps_dir, "lib64"),
                                          os.path.join(krita_deps_dir, "lib/x86_64-linux-gnu")])

    add_paths_to_env( "LIBRARY_PATH", [os.path.join(krita_install_dir, "lib"),
                                       os.path.join(krita_install_dir, "lib64"),
                                       os.path.join(krita_deps_dir, "lib"),
                                       os.path.join(krita_deps_dir, "lib64")] )

    add_paths_to_env( "PKG_CONFIG_PATH", [os.path.join(krita_deps_dir, "lib/pkgconfig"),
                                          os.path.join(krita_deps_dir, "lib64/pkgconfig"),
                                          os.path.join(krita_install_dir, "lib/pkgconfig"),
                                          os.path.join(krita_install_dir, "lib64/pkgconfig")])

    add_paths_to_env( "CMAKE_PREFIX_PATH", [krita_deps_dir])

    add_paths_to_env( "PYTHONPATH", [os.path.join(krita_deps_dir, "sip")])

    os.environ["ASAN_OPTIONS"] = "detect_leaks=0"
    

    # MLT related environment variables...
    os.environ["MLT_REPOSITORY"] = os.path.join(krita_deps_dir, "lib/mlt-7/")
    os.environ["MLT_DATA"] = os.path.join(krita_deps_dir, "share/mlt-7")
    os.environ["MLT_ROOT_DIR"] = krita_deps_dir
    os.environ["MLT_PROFILES_PATH"] = os.path.join(krita_deps_dir, "share/mlt-7/profiles/")
    os.environ["MLT_PRESETS_PATH"] = os.path.join(krita_deps_dir, "share/mlt-7/presets/")
