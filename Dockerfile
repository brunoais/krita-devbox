FROM ubuntu:18.04

MAINTAINER Eoin ONeill <eoinoneill1991@gmail.com>

# Avoiding tzdata configuration popups..
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata

# Download newest CMake version by setting up Kitware PPA..
RUN apt-get update
RUN apt-get install -y gpg wget
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
RUN apt-get update && apt-get install kitware-archive-keyring


# Download all krita dependencies.. 
RUN apt-get update \
    && apt-get install -y build-essential cmake git locales \
            vim curl gdb valgrind sysvinit-utils libxml-parser-perl \
            extra-cmake-modules locate gettext \
            libpq-dev libaio-dev gperf libasound2-dev libatkmm-1.6-dev \ 
            libbz2-dev libcairo-perl libcap-dev libcups2-dev libdbus-1-dev \
            libdrm-dev libegl1-mesa-dev libfontconfig1-dev libfreetype6-dev \
            libgcrypt20-dev libgl1-mesa-dev libglib-perl libgsl0-dev libgsl0-dev \
            gstreamer1.0-alsa libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
            libgtk3-perl libjpeg-dev libnss3-dev libpci-dev libpng-dev libpulse-dev \
            libssl-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev \
            gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly \
            libtiff5-dev libudev-dev libwebp-dev flex libmysqlclient-dev libx11-dev \
            libxkbcommon-x11-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-util0-dev \
            libxcb-res0-dev libxcb1-dev libxcomposite-dev libxcursor-dev libxdamage-dev \
            libxext-dev libxfixes-dev libxi-dev libxrandr-dev libxrender-dev libxss-dev \
            libxtst-dev mesa-common-dev libffi-dev liblist-moreutils-perl libtool \
            libpixman-1-dev subversion gdb valgrind sysvinit-utils mesa-utils libxcb-icccm4 \
            libxcb-image0 libxcb-render-util0 libxcb-xinerama0 python3 python3-dev \
            meson ninja-build libicu-dev


# Download latest GCC/G++ using ubuntu toolchain..
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:ubuntu-toolchain-r/test
RUN apt-get update
RUN apt-get install -y gcc-11 g++-11


# Update alternative library options..

## GCC
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 10 && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7  20 

## G++
RUN update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-11 10 && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7  20 


# Get AppImage building dependencies..
RUN apt-get install -y fuse rsync patchelf

# .. Including linuxdeployqt.. 
RUN wget -c -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage" -O /bin/linuxdeployqt
RUN chmod a+x /bin/linuxdeployqt

RUN locale-gen en_US en_US.UTF-8

# Install Krita audio dependencies for MLT building..
RUN apt-get install -y libvorbis-dev nasm

# Install Krita Documentation dependencies..
RUN apt-get install -y python3-sphinx

# Install Krita Development Fund dependencies..
RUN apt-get install -y python3-pip libmariadb-dev
RUN pip3 install --upgrade pip

# Setup dependencies for `krita-devbox` script..
RUN apt-get install -y python3-click

# Install custom scripts and configurations..
COPY ./bin/krita_env.py \
     ./bin/krita-build-appimage \
     ./bin/krita-build-dependencies \  
     ./bin/krita-cmake \
     ./bin/krita-devbox-setup \
     ./bin/krita-devbox \
     /bin/

COPY ./bin/krita-dev-environment.inc \
     /opt/

# Setup Krita environment variables. 
ENV KRITA_DIR=/var/krita
ENV KRITA_BUILD_DIR=${KRITA_DIR}/build
ENV KRITA_INSTALL_DIR=${KRITA_DIR}/appdir/usr
ENV KRITA_DEPS_DIR=${KRITA_DIR}/deps/usr

# Setup Krita build directories and symlinks.
RUN mkdir -p ${KRITA_DIR}/appdir/usr && \
    ln -s ${KRITA_DIR}/appdir ${KRITA_DIR}/krita.appdir && \
    mkdir -p ${KRITA_DIR}/build && \
    ln -s ${KRITA_DIR}/build ${KRITA_DIR}/krita-build && \
    mkdir -p ${KRITA_DIR}/deps/usr

# These need to be assigned to get Click to work as a distrobox init-hook
ENV LC_ALL=en_US.utf-8
ENV LANG=en_US.utf-8

RUN chmod -R 757 /var/krita
RUN /bin/bash -c "source /opt/krita-dev-environment.inc"
