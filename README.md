⚠️ Warning: Krita Devbox is considered an *experimental* way of building and developing for Krita. ⚠️

# Krita Devbox

A complete Krita development environment in a box!

Based on [Distrobox](https://github.com/89luca89/distrobox) (which manages Docker/Podman containers), Krita Devbox is intended to provide a consistent and easy-to-use development container that comes with everything you need to develop Krita, write documentation, produce AppImages, and so on. A key goal for Krita Devbox is to be as simple to setup and use as possible by integrating with the host system (when it makes sense), and otherwise exporting a flexible, human-readable interface to the host system.

Also, while Krita Devbox is being made with atomic operating systems like Fedora Kinoite/Silverblue in mind, we hope it'll be useful in the future to anybody who is interested in developing for Krita.

## Initial Setup

0. Krita Devbox is primarily designed to be run with [Distrobox](https://github.com/89luca89/distrobox) (which wraps Docker/Podman containers) so you will need to install Distrobox, as well as Docker and/or Podman. If your system is relatively recent you should be able to simply install Distrobox and its dependencies from your Linux distribution's repository, but you can [read the Distrobox installation instructions](https://github.com/89luca89/distrobox#installation) for other options.

1. Once you have Distrobox installed, open a terminal and run `distrobox create --image eoinoneill1991/krita-devbox --pull --home ~/.krita-devbox` . (If you're presented with container repository options, pick `docker.io/eoinoneill1991/krita-devbox:latest`.) This step will create your Krita Devbox, while giving it a separate home folder hidden in `~/.krita-devbox` on your host system. (Note: You *will* still have access to your host system's $HOME from within the devbox.) 

2. Next, run `distrobox enter krita-devbox -- krita-devbox-setup` . This command will enter your Krita Devbox and run our setup script, which is used to download Krita's latest prebuilt dependencies and do any other first-time configuration.

3. Finally, run `distrobox enter krita-devbox -- 'distrobox-export --bin /bin/krita-devbox --export-path ~/.local/bin'` . This command exports a helpful utility script called `krita-devbox` to your host OS (at the location on your host OS that's specified by the `--export-path` option--ideally somewhere in your hosts OS's $PATH). This script makes it easy to do common Krita development tasks within the devbox without needing to manually enter it. You'll find details about how to use the `krita-devbox` script in the sections below.

**NOTE (especially for NVidia GPU users):** You should have the same GPU driver inside your container as you have on the host system. In other words, AMD GPU users who use the in-kernel driver don't need to worry about it, but if you're an NVidia GPU owner and running the proprietary `nvidia` driver on your host system, you may need to enter your devbox (see the Advanced Usage section below) and install the same driver inside the container.

And that should be it! These three steps will hopefully be all you need to set up a complete Krita development container on just about any recent Linux machine. 

## Basic Usage with `krita-devbox`

If you followed all of the initial setup insructions above, you should now have a script called `krita-devbox` in your host OS's path. (If you *can't* run `krita-devbox` from a terminal on your host OS then you've either skipped the exporting step or exported to a location that's not in your system's $PATH) This script is designed to provide a simple interface for doing commonly useful things (like building binaries and AppImages, running tests, generating docs, etc.) with Krita Devbox from the comfort of your host OS. :)

From your host OS you can run:

#### `krita-devbox --help`

Prints out a detailed description of every subcommand and associated options. Using the `--help` option in conjunction with a subcommand (for example, `krita-devbox build --help`) will print out a help page describing the various subcommand-specific options. 

#### `krita-devbox build [/path/to/krita/source]`

Configure and build Krita within the container. 

Adding the `--appimage` or `-a` option will also pack the binary into a portable Linux [AppImage](https://appimage.org/) format. Adding the `--fresh` or `-f` option will clean the internal build and install directories for a complete rebuild. Adding the `--dependencies` or `-d` option will also build Krita's dependences from source. Finally, many options (like `--appimage` or `-a`) have corresponding negative options (like `--no-appimage` or `-A`) to allow for as much explicit control as possible.

(The path to Krita's source code should be wherever Krita's source code is on your system, typically some location in your user $HOME as it is accessible from inside the Krita Devbox by default. If you don't have Krita's source code on your computer yet, you'll need to either `git clone` it manually from https://invent.kde.org/graphics/krita or run `krita-devbox clone /path/to/krita/project/dir`.)

#### `krita-devbox run`

Run the Krita that was built with `krita-devbox build /path/to/krita/source` from within the Devbox container. Adding the `--debug` option will run Krita through GDB for debugging. Adding the `--test` option will first run Krita's suite of unit tests.

#### `krita-devbox build-docs [/path/to/krita/documentation]`

Generates a local HTML version of Krita's [User Manual](https://docs.krita.org/en/index.html) which can be opened in a browser for previewing documentation changes. Building documentation locally is also useful for seeing errors and warnings which aren't visible when editing through the web interface.

#### `krita-devbox clean`

Cleans up existing build and install directories inside the Devbox container. Optional flags are available to control which directories will and wont be cleaned.

#### `krita-devbox goto [subcommand]`

Opens the default web browser on the host system and navigates to a commonly-used Krita development site. For example `krita-devbox goto bugs` jumps to Krita's bug tracker, and `krita-devbox goto bug 123456` would go to bug report #123456. Similarly, `krita-devbox goto invent` browses to https://invent.kde.org, while `krita-devbox goto mr 1234` would open merge request #1234. 

To name a few more, `docs` navigates to the Krita User Manual, `devfund` to https://fund.krita.org, `artists` to Krita-Artists, etc.

## Reinstalling or Updating Krita-Devbox

If your build within krita devbox fails or you have any other problems, it's likely your devbox needs to be updated. If you need to update or simply reinstall your devbox, the following commands should get the job done.

```
distrobox stop krita-devbox
distrobox remove krita-devbox
distrobox create --image eoinoneill1991/krita-devbox --pull --home ~/.krita-devbox
distrobox enter krita-devbox -- krita-devbox-setup
```

This should pull the latest image from dockerhub. Stopping and removing the devbox might not always be necessary, but it's a good idea to reduce the number of unused containers on your system. 

## Advanced Usage with Distrobox

While the goal of the `krita-devbox` script is to make it easy to use Krita Devbox from your host OS, we aren't limited to working that way! 

By opening a terminal and running the command `distrobox enter krita-devbox` you can easily enter into the container, giving you access to and total control over the Krita Devbox. 

While you can use the Krita Devbox like any other Ubunutu Linux system, it's important to be aware of the fact that (unlike a regular Docker/Podman container) Distrobox containers share your host system's $HOME folder by default! Distrobox is really powerful tool, [so I recommend checking out their great documentation for more information about how it works and what it can do.](https://github.com/89luca89/distrobox)

## Tips & Tricks

- If you created your Krita Devbox using the steps above, you should have a seprate $HOME directory for your development environment located at `~/.krita-devbox` on your host system. This can be helpful to prevent clutter from accumulating in the host's $HOME, as well as preventing potential conflicts between multiple distrobox (or Krita Devbox) containers. If you'd prefer to have your container share a $HOME directory with your host system, you can omit the `--home ~/.krita-devbox` option from the initial `distrobox create` step. Keep in mind that the host OS's $HOME is accessible from inside the container either way.

- During the initial setup described above, we run an internal script called `krita-devbox-setup`. This script needs to be run at least once in order to finish setting up our Krita development environment. One part of that is downloading a reasonably large package of dependencies needed to create Krita AppImages. However, it is also possible to keep a single copy of those dependencies on your host system and supply them directly to the script by instead assigning something like `krita-devbox-setup --deps-path /path/to/your/deps.tar` as the init hook. (The path should be somewhere on the host system that's accessible from within the Devbox container, like $HOME.) Most people probably won't use this, but it can be a time and bandwidth saver for anybody who anticipates that they might frequently want to make Krita Devbox containers.

- You can use `distrobox list` to get a list of your existing containers and, should you decide to discard your current Krita Devbox and start from scratch, you can easily do that by running `distrobox rm krita-devbox` (or whatever your container is named) before once again following the initial setup steps above.

- Running `krita-devbox build -aBC /path/to/krita/source` will package an AppImage without first running cmake or building. Useful for quickly packaging an existing build for community testing. (It's as easy as ABC! But it probably won't do anything useful if you don't have a working build in the container...)

- This readme kind of assumes you already have Krita's source code repository cloned somewhere, but if you're setting up your Krita development environment totally from scratch, running `krita-devbox clone /path/to/krita/project/dir` is a shorthand to `git clone` both Krita's source code (into a subdirectory called `source`) and Krita's documentation (into a subdirectory called `docs`). However, you will probably still need to point both repositories to your own remote forks on https://invent.kde.org in order to submit merge requests.

## Troubleshooting

- Is your Krita failing to launch when you run within the devbox (either by entering into it or running `krita-devbox run` from the host)? Are you seeing immediate errors relating to OpenGL, Vulkan, llvmpipe, or similar? If so, it's likely that you have a host-container driver mismatch. Make sure that you are running the same driver on your host system as you are inside the devbox! `nvidia` on the host? `nvidia` in the devbox please!

- Krita runs, but all of my UI text is totally missing, what the hell? In my experience, this is likely caused by a `fontconfig` conflict between a host OS and devbox container when sharing a $HOME directory. Whether sharing a $HOME causes problems or not depends heavily on the host distro and configuration, but if you run into weird behavior or unexpected issues when running a shared-home devbox setup it's probably worth trying a separate-home configuration. (`--home /path/to/container/home` during the creation step.)

## Contribution

Feel free to give us feedback or submit merge requests to this project if you have ideas for how we can further improve the Krita Devbox workflow. :) 




